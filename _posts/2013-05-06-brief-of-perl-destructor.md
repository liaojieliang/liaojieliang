---

layout: post

title: Perl DESTROY函数简介

categories: software-development

---

当一个对象最后一个引用也失效时，这个对象就会被自动销毁。也就是说，系统会帮你做一些清理工作。但是有时候想要在对象销毁时做一些额外的事情，设定一个函数让其自动执行，有点像回调函数那样。此时你可以为这个类定义一个叫DESTROY的函数，函数里做你想在对象销毁时做的事情。

例：

```
sub DESTROY {
    my $self = shift;
    print "object is destroyedn";
    return;
}
```

注意，这个函数只接收自身类一个参数，也就是上面的 $self。当然，通过这个$self就可以访问到对象的私有数据。

由于你不知道DESTROY会在什么时候被调用，所以当它操作全局变量时要特别小心。

还有一点需要注意的是，如果DESTROY函数内部抛出异常，它不会被打印到错误输出，也不会引起程序终止。但如果使用了eval{ }，$@是会被设置的，而$@是全局的，所以也要小心。

参考：[perldoc](http://perldoc.perl.org/perlobj.html#Destructors)

2013-05-06-杰良
