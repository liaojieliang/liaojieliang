---

layout: post

title: "使用 netcat 实现反向 shell 访问以便于远程调试设备"

---

# 问题

在实际开发项目中，原型产品在推向市场、实际部署使用之前，也需要在实际环境进行试验性运行，以便测试和改进。

但可能出现的一个问题是，部署测试的实际环境与办公室有一定的距离，甚至在不同的城市里。当应用服务程序异常后者挂掉的时候，就没有办法探查设备的实际情况了。

要是在办公室里，就可以基于局域网 ssh 登录访问，甚至基于调试串口登录。由于设备在远程，是无法通过 ssh 直接登录了。

# 基本概念

我们上面说的“登录”，其实都是是指通过 Linux 的 shell 登录系统。那下面准备介绍的反向 shell 就是基于 Linux shell 的一种技术。这里先介绍这个概念以及相似的 bind shell 概念。

## Bind Shell

bind shell 是用户用 bash，将 shell 绑定到一个本地端口上，这样外部系统就可以基于网络往这里发送 shell 命令。自己这里是作为服务端接收命令。但请注意，这需要外部系统有访问这个系统的网络支持，所以一般是在同一个局域网内。

## Reverse Shell

反向 shell 是把自己作为客户端，向特定的远程系统发送自己的 shell。虽然在底层上也是通过网络接收 shell 命令并执行，但是这样的“反向”方式适用于不同的网络环境。作为网络通信的发起者，本地终端设备可以主动连接远程服务器，让服务器“登录”上来，执行 shell 命令。

# 解决方法

## netcat 的编译

下载源码：[https://sourceforge.net/projects/netcat/](https://sourceforge.net/projects/netcat/)

### 用于 x86 平台的编译

```
./configure --prefix=/home/jack/others/netcat-0.7.1/netcat
make
make install
```

### 用于 ARM 平台的编译（以 OMAPL138 为例）

```
source /home/jack/omapl138/ti/mcsdk_1_01_00_02/linux-devkit/environment-setup
./configure --prefix=/home/jack/others/netcat-0.7.1/netcat --host=arm-arago-linux-gnueabi
make
make install
```

生成的文件在当前目录下 netcat 里。

## netcat 的使用

### 先在服务端启动监听

```
nc -l -p 8089 -vvv
```

8089 端口号可随意指定一个未被使用的。

### 在设备端发起反向 shell

```
./netcat/bin/netcat -e /bin/sh 192.168.1.60 8089
```

IP 为服务端 IP，端口号与服务端监听的一致。这里 IP 是服务器的公网 IP。

此时就可以在服务端让设备端执行 shell 命令了。

## 异步发起反向 shell

在项目实践中我们使用了 MQTT，客户端与服务端保持连接。然后我们就让每台设备订阅对应的反向 shell 主题，服务端就可以在需要的时候发送某台设备对应的主题，让其发起一个 netcat 反向 shell 连接了。当然，服务需要事先启动对应的 netcat 监听端口。

这样，就相当于登录了这台设备，可以进行必要的调试操作了。

# 参考
[https://blog.csdn.net/jb19900111/article/details/17756203](https://blog.csdn.net/jb19900111/article/details/17756203)

*廖杰良 - 2018-9-4*
