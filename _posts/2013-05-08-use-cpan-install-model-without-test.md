---

layout: post

title: "使用cpan安装Perl模块免去测试"

categories: software-development

---

我在使用cpan安装SDL模块的时候，遇到了一个bug，某一个测试文件执行了几个小时，测试数量达到了五十多万。后来就终止了这次安装，寻找免去测试的安装方法。

我先是man了cpan，发觉没有不测试的可选参数。于是google之，发现一个叫cpanm的命令。随即apt-get安装，却被告知源里没有提供这个软件，可能找的是cpanminus。于是到package.ubuntu.com搜索这个cpanminus，看了介绍就明白了，大致是cpan的一个扩展版本。

```
# sudo apt-get install cpanminus
```

同时也可以通过cpan安装：

```
# sudo cpan App::cpanminus
```

免去测试直接安装的方法：

```
# sudo cpanm -n Test::More
```

-n表示notest，更详细的可以man一下cpanm查看。

我这次时为了绕开一个bug而使用免去测试的方法，另外的原因可能是比较节省时间。不过，cpan时基于源码安装的，安装之前进行测试是比较必要的。

[这篇文章](http://www.openswartz.com/2012/01/31/stop-running-tests-on-install/)对不应默认测试进行了讨论。

2013-05-08-杰良
