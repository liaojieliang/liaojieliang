---

layout: post

title: C、C++ 实现回调函数

categories: software-development

---

### 1、对于 C 语言

一个及其简单的例子，其实就是利用了函数指针进行调用。

```
#include<stdio.h>
void call(int a, int b, void (*callback)(int c)) {
    callback(a+b);
}
void call_back(int c) {
    printf(&quot;%d\n&quot;, c);
}
void main(void) {
    call(1, 2, call_back);
}
```

### 2、对于 C++ 语言的静态成员函数

C++ 的静态函数与 C 语言的签名是一样的，所以回调函数的实现也是一样。

### 3、C++ 语言的非静态函数作为回调函数

由于 C++ 语言类的实现问题，对象的非静态成员函数的引用方式跟 C 函数不同，所以作为回调函数时需要特别的调用方式。简单来说就是把对象的地址传出去，通过地址来间接调用成员函数。

示例：

```
class ClassA {
/* 真正的回调函数 */
void display(const char *text) {
    cout << text << endl;
}
/* 通过这个静态函数间接调用回调函数，需要传递具体某个对象的指针给它 */
static void wrapper_to_call_display(void ptr2object, char *text) {
    ClassA *ptr = (ClassA*)ptr2object;
    ptr->display(text);
}
};
/* 此函数使用用户定义的回调函数 */
void call(void *ptr2object, void (*callback)(void *ptr2object, char *text)) {
    callback(ptr2object, /* pass this to callback function */);
}
/* 创建类 ClassA 的实例，调用 call 触发回调函数 */
void do_it() {
    ClassA objA;
    /* 把静态函数“注册”到 call 函数。注意，函数名后不跟括号 */
    call((void*)&objA, ClassA::wrapper_to_call_display);
}

```

2013-11-16-杰良
