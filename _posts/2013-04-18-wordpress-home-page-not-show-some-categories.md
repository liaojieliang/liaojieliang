---

layout: post

title: "设置wordpress首页不显示某一个分类下的文章"

categories: software-development

---

直接修改代码的方法：
首先，在本地进入正在使用的主题的目录，打开index.php，找到这一行代码：

```
<?php if ( have_posts() ) : ?>
```

然后把它改为这样：

```
<?php if ( have_posts() ) : is_home() && query_posts($query_string . '&cat=-126') ?>
```

其中126为分类ID，减号表示排除，不显示。这个ID的查看，可以在网页后台控制面板，把鼠标置于相应分类，浏览器左下角就会有显示。大致会有包含以下字样：tag_ID=17 。这里17就是对应分类的ID了。最后保存、更新代码，大功告成。

我会写比较多的个人日志，都是些没什么营养的东西，所以不想把它们显示在首页，碍人视线。但如果有人想要看的话还是可以打开对应分类来查看的。所以这个方法很有用。

参考：[http://www.xu-le.com/knowledge-2/325](http://www.xu-le.com/knowledge-2/325)

2013-04-18-杰良
