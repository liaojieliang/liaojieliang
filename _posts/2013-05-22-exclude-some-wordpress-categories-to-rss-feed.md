---

layout: post

title: "排除Wordpress特定分类文章在 RSS feed 中显示"

categories: software-development

---

RSS 是个很好的东西，通过它聚合一些高质量的文章，方便阅读和收藏。通过给别人订阅，也能够促进交流。所以，不高质量的东西就不好意思feed给别人了。又或者有些分类下的文章，不便于扩散出去。就拿我自己来说，我有一个个人日志分类，都只是自己的唠叨。于是决定把它从feed中分离出来。好了，废话不说了。下面介绍插件和修改源码两种方法。

1、添加插件 Advanced Category Excluder

这是一个比较强大的插件，主要是进行内容管理。其中就有一项关于feed的。启用插件后，在仪表盘左侧栏的差不多最下面的地方或生成一个“ACE”条目，这就是关于这个插件的。下载地址：[wordpress官网](http://wordpress.org/plugins/advanced-category-excluder/)

2、修改源码的方法

可以通过手动添加下面的代码来实现:

```
http://example.com/feed?cat=-5
```

需要排除多个分类，只需要在中间使用 & 字符隔开:

```
http://example.com/feed?cat=-4&cat=-5
```

如果你对上面的两个方法都不满意，你还可以使用下面的方面，打开function.php文件，粘贴下面的代码:

```
function myFilter($query) {
    if ($query->is_feed) {
        $query->set('cat','-5');//别忘了更改分类ID =^o^= 
    }
    return $query;
 }

add_filter('pre_get_posts','myFilter');
```

参考：[九个实用的Wordpress RSS技巧](http://www.wordpress.la/9-useful-rss-tricks-for-wordpress.html)

2013-05-22-杰良
