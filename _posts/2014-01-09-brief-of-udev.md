---

layout: post

title: "最近的有关 udev 的认识"

categories: software-development

---

udev 是 Linux 内核的设备管理器。总的来说，它负责管理 /dev 目录以及用户空间中所有的硬件添加和删除操作。

开始去学习这个东西是为了实现这样的一个需求：但用户随机插入和拔出移动存储器时，我的程序能够马上知道，并进行相应的改变以应对变化。具体来说就是当有新的 U 盘接入时，我就把 U 盘作为一棵子树并入到总的歌曲列表里，移除时就移除相应的子树或者终止真正播放。所以我需要的是一个系统级的实时通知。之前考虑的一个 dirty 的方法是，在程序里轮询挂载目录，有新增目录就进行添加，有减少就进行移除。但这个方法太笨拙。

udev 通过编写规则（rules）文件进行定制。规则文件必须以 rules 作为后缀，系统管理员编辑的习惯放于 /etc/udev/rules.d/ 中，其它软件包提供的规则则存放于 /lib/udev/rules.d 中。如果两个目录中有同名的规则文件，则只有 /etc/udev/rules.d 中的有效。而对于所有的规则文件，udev 按照文件顺序来进行解释执行，有先后关系。习惯以数字作为开头来命名规则文件，e.g. 10-usb-storage.rules 。

### udev 工作流程：

![workflow](/images/2014/udev_workflow.jpg) 

### 关于规则文件的编写

在规则文件里，除了以“#”开头的行（注释），所有的非空行都被视为一条规则，但是一条规则不能扩展到多行。规则都是由多个键值对（key-value pairs）组成，并由逗号隔开，键值对可以分为条件匹配键值对和赋值键值对，一条规则可以有多条匹配键和多条赋值键。匹配键是匹配一个设备属性的条件，当一个设备的属性匹配了该规则里所有的匹配键，就认为这条规则生效，然后按照赋值键的内容，执行该规则。

### 支持的规则操作符

“==”：比较键、值，若等于，则该条件满足；“!=”： 比较键、值，若不等于，则该条件满足；“=”： 对一个键赋值；“+=”：为一个表示多个条目的键赋值；“:=”：对一个键赋值，并拒绝之后所有对该键的改动，目的是防止后面的规则文件对该键赋值。

### 规则的匹配键

ACTION：事件 (uevent) 的行为，例如：add( 添加设备 )、remove( 删除设备 )。

KERNEL：内核设备名称，例如：sda, cdrom。

DEVPATH：设备的 devpath 路径。

SUBSYSTEM：设备的子系统名称，例如：sda 的子系统为 block。

BUS：设备在 devpath 里的总线名称，例如：usb。

DRIVER：设备在 devpath 里的设备驱动名称，例如：ide-cdrom。

ID： 设备在 devpath 里的识别号。

SYSFS{filename}：设备的 devpath 路径下，设备的属性文件“filename”里的内容。例如：SYSFS{model}=="ST936701SS" 表示：如果设备的型号为 ST936701SS，则该设备匹配该匹配键。

ENV{key}：环境变量，进行引用。

PROGRAM：调用外部命令。

RESULT： 外部命令 PROGRAM 的返回结果。

### 重要的赋值键

NAME：在 /dev 下产生的设备文件名。只有第一次对某个设备的 NAME 的赋值行为生效，之后匹配的规则再对该设备的 NAME 赋值行为将被忽略。

SYMLINK：为 /dev/ 下的设备文件产生符号链接。

OWNER, GROUP, MODE：为设备设定权限。

ENV{key}：设置一个环境变量，可以给稍后的规则使用。

### 规则的替换操作符：</h3>

$kernel, %k：设备的内核设备名称，例如：sda、cdrom。

$number, %n：设备的内核号码，例如：sda3 的内核号码是 3。

$devpath, %p：设备的 devpath路径。

$id, %b：设备在 devpath里的 ID 号。

$sysfs{file}, %s{file}：设备的 sysfs里 file 的内容。其实就是设备的属性值。 例如：$sysfs{size} 表示该设备 ( 磁盘 ) 的大小。

$env{key}, %E{key}：一个环境变量的值。

$major, %M：设备的 major 号。

$minor %m：设备的 minor 号。

$result, %c：PROGRAM 返回的结果。

$parent, %P：父设备的设备文件名。

$root, %r：udev_root的值，默认是 /dev/。

$tempnode, %N：临时设备名。

### 参考

[使用 udev 高效、动态地管理 Linux 设备文件](http://www.ibm.com/developerworks/cn/linux/l-cn-udev/)

[Writing udev rules](http://www.reactivated.net/writing_udev_rules.html)

2014-01-09-杰良
