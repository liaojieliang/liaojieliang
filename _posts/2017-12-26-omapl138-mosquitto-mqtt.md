---

layout: post

title: "移植 MQTT broker mosquitto 到 omapl138"

---

## 概述

本次移植使用创龙的 TL138-EVM 开发板，基于 TI 的 MCSDK 开发环境。具体上位机系统为 Ubuntu 12.04-32bit，软件环境基于创龙的用户手册搭建。

为了移植后在 138 上运行的 web 服务也能使用 mosquitto，所以编译构建时要开启其 libwebsocket 功能支持。所以这里移植就总共分三步走。交叉编译 libwebsocket，交叉编译 mosquitto，移植编译结果到目标文件系统。

## 交叉编译 libwebsocket
### 准备工作
官网下载源码包：[https://libwebsockets.org/](https://libwebsockets.org/)

构建过程需要比 Ubuntu 的 apt-get 方式安装的更高版本的 cmake 工具，可以参考这里进行安装：[https://askubuntu.com/questions/610291/how-to-install-cmake-3-2-on-ubuntu-14-04](https://askubuntu.com/questions/610291/how-to-install-cmake-3-2-on-ubuntu-14-04)

### 编译过程
 * 进入解压后的源码包 cd libwebsockets-master
 * 创建 build 目录。mkdir build
 * 编辑交叉编译 cmake 文件。vim ../cross-arm-linux-gnueabihf.cmake
 * 修改文件中的路径变量，指向 MCSDK对应位置。 CROSS_PATH, CMAKE_FIND_ROOT_PATH, CMAKE_INSTALL_PREFIX
 * source MCSDK 开发环境。source /home/jack/omapl138/ti/mcsdk_1_01_00_02/linux-devkit/environment-setup
 * 执行 cmake。cmake  ..  -DCMAKE_INSTALL_PREFIX:PATH=/usr  -DCMAKE_TOOLCHAIN_FILE=../cross-arm-linux-gnueabihf.cmake  -DLWS_WITHOUT_EXTENSIONS=1  -DLWS_WITH_SSL=0
 * 编译并提取生成的文件。make && make install 。生成的文件提取到 websockets-lib 目录。

上述修改 cmake 文件中的三个变量的参考：
```
set(CROSS_PATH /home/jack/omapl138/ti/mcsdk_1_01_00_02/linux-devkit/sysroots/i686-arago-linux/usr )

set(CMAKE_FIND_ROOT_PATH "${CROSS_PATH}" "/home/jack/omapl138/ti/mcsdk_1_01_00_02/linux-devkit/sysroots/armv5te-3.3-oe-linux-gnueabi")

set(CMAKE_INSTALL_PREFIX "/home/jack/project/sinoyue-138/mosquitto/libwebsockets-master/build/websockets-lib/")
```
 
## 交叉编译 mosquitto
### 准备工作
官网下载源码包：[http://mosquitto.org/download/](http://mosquitto.org/download/)

### 编译过程
 * 解压并进入源码包。 cd mosquitto-1.4.14
 * 编辑 config.mk 配置前面交叉编译出来的 libwebsocket 的头文件和库的路径。
 * source MCSDK 开发环境。source /home/jack/omapl138/ti/mcsdk_1_01_00_02/linux-devkit/environment-setup
 * 编译。make WITH_SRV=no WITH_WEBSOCKETS=yes WITH_TLS=no CROSS_COMPILE=arm-arago-linux-gnueabi- CC=gcc CXX=g++ AR=ar
 * 提取生成的文件。mkdir build  &&  make install DESTDIR=/home/jack/project/sinoyue-138/mosquitto/mosquitto-1.4.14/build

上述修改 config.mk 的示例如下：
```
ifeq ($(WITH_WEBSOCKETS),yes)
    BROKER_CFLAGS:=$(BROKER_CFLAGS) -DWITH_WEBSOCKETS -I/home/jack/project/sinoyue-138/mosquitto/libwebsockets-master/build/include
    BROKER_LIBS:=$(BROKER_LIBS) -lwebsockets -L/home/jack/project/sinoyue-138/mosquitto/libwebsockets-master/build/lib
endif
``` 

## 安装到 138 文件系统
### 安装 libwebsocket
 * 把生成的 websockets-lib 传输到 138 文件系统。
 * 安装。cp -r websockets-lib /usr
 * sync

### 安装 mosquitto
 * 把生成 build 文件夹传输到 138 文件系统。
 * 安装配置文件。cp -r ./build/etc / 
 * 安装头文件和库文件等。 cp -r ./build/usr/local /usr
 * sync

## 简单试验
在三个不同终端中依次执行以下操作：
 * 启动 broker。mosquitto
 * 订阅。mosquitto_sub -t 'test/topic' -v
 * 发布。mosquitto_pub -t 'test/topic' -m 'hello world'

发布之后即可看到订阅的终端收到发布的消息。

至此，大功告成！

2017-12-26
