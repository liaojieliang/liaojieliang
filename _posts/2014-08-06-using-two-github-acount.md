---

layout: post

title: "同时使用两个 github 账号"

categories: software-development

---

最近看到一个关于 github 的笑话：
![关于 github 的笑话](/images/2014/github_joke.jpg)

LOL

从中可见程序员们对 github 的喜爱之情，溢于言表！

说回正题，拥有两个账号的一个问题是，认证。如果是使用浏览器，你可以左手 Chrome 右手 Safari 开两个，分别记录账号密码。如果你十分钟爱 Chrome，非它不用，也可以让它帮你同时记住两个账号密码。切换的时候如果它帮你自动填写的账号不是你想要使用的那个，只要你稍微打上去你账号的头两个字母，亲爱的 Chrome 就明白你想要干嘛了。赞！只是不能**同时**使用这两个账号。不过其实这种情况极少发生吧。

使用 github 的最重要的途径不是浏览器，是 Git（废话！）。Git 通过 ssh-key 来认证十分方便，大家都知道。只要复制本地生成的 public key，打开你的 github 账号就可以进行设置了。但问题是 github 不让用同一个 public key 设置不同的账号。这个是为什么我没想明白。不过后来我一拍脑袋，“哦，是因为安全的原因”（又废话！）。这好办，再生成一个 public key 呗！对于这个，网络上已经有太多的文章啦，可以看看[这里](https://gist.github.com/suziewong/4378434)。

我嫌这个办法麻烦。别忘了！Git 除了通过 ssh-key 认证到 github，还可以使用 https 的！只要你 clone 的时候使用的是 https URL，push 时就是通过 https 方式。Git 会问你要 github 账号和密码，输入一次之后它就会帮你记住啦，不用每次 push 都输账号密码。至于多久会失效需要重新输入我也不清楚，不重要！什么？你不想 git 记住，要当前的账号认证马上失效？也不是没有办法。看过来：
```
git credential-osxkeychain erase
host=github.com
protocol=https
```

终端执行完这个就高枕无忧了！坏消息是[该方法](https://help.github.com/articles/updating-credentials-from-the-osx-keychain)适用于 Mac OSX，其他系统还不清楚要怎么做。

这样，我一个账号通过 ssh-key 认证，另一个通过 https。几近完美的解决方案。^_^

杰良-2014-08-06
