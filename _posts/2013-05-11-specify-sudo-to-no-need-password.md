---

layout: post

title: "配置让sudo操作不必输入密码"

categories: software-development

---

在使用Linux系统的时候经常要通过 sudo 来执行某些命令，然后就要敲入用户密码。这个密码会被保存，但时间只有5分钟。但怎么样做到不必再输入密码呢？

关键在于 /etc/sudoers 文件。这个文件实在是太重要了，还有一个专门的编辑命令 visudo 。执行 $ sudo visudo ，输入用户密码就可以进行编辑了。不过它是启用默认编辑器（我的系统里是nano编辑器），并不好用。可以直接执行 $ sudo vim /etc/sudoers 。会看到大致如下内容：

```
Defaults	env_reset
Defaults	mail_badpass
Defaults	secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

root	 ALL=(ALL:ALL) ALL

# Members of the admin group may gain root privileges
%admin ALL=(ALL) ALL

# Allow members of group sudo to execute any command
%sudo 	ALL=(ALL:ALL) ALL
```

为了达到目的，就在行 %admin ALL=(ALL) ALL 下面加入如下一行：

```
username ALL=NOPASSWD:ALL
```

注意，这行如果写在%admin 那行的前面，就很可能会失效。因为被后面admin组的设置覆盖了。

当然，这样做很不安全。可以改为设置某一个或几个命令不需要密码，其他的不变。把上面的一行该为：

```
username ALL=NOPASSWD:ALL:/bin/ls,NOPASSWD:/bin/mount
```

这样，执行 $ sudo ls 和 $ sudo mount 就不需要输入用户密码了。这样做确实是方便了一点。但这个方法更有用的地方在于，在实现一些自动化运行的时候，需要用到root权限，但使用sudo却不方便手动输入密码（因为程序是自动化执行的）。这个时候就可以用上面的方法配置这个程序。

2013-05-11-杰良
