---

layout: post

title: "带滑动效果的 Qt 界面切换"

categories: software-development

---

## 唠嗑
现在的软件，都很讲究用户体验。即使是没有图形界面的软件，比如说伟大的 git，升级之后大大增强了用户交互性。使用的时候如果输错了命令，它会友情提示你，小弟你想要的实际是这个命令吧？

虽然并不是所有用户体验都关乎图形界面，但是图形界面无疑是用户首先关注的，也是普通用户最为关注的。赏心悦目的图形界面，可以立即拉近软件和用户的关系。

## 干货
通过 google 我找到了诺基亚网站上的这篇[文章](http://developer.nokia.com/community/wiki/Extending_QStackedWidget_for_sliding_page_animations_in_Qt)，正是我需要的。噢，感谢谷歌，感谢诺基亚。

它基于 QStackedWidget 进行增强，使用 stack 来管理多个页面，然后使用 Qt 的 Animation 相关组件来实现两个页面的滑动效果。通过设置 QEasingCurve::Type 值，可以直接设置多种滑动效果，很赞！打开 QEasingCurve 的官方文档，可以看到每一个 Type 值对应一个曲线图。其实就是时间和位置的关系图，学过高中数学的人都可以从曲线图想象出大致的滑动效果。很有趣。

我把 nokia 上的代码稍稍进行了重构，放到了 [github](https://github.com/jackindata/SlidingStackedWidget) 上。欢迎取用参考。

## 延伸
最近在考虑使用堆栈做一个页面管理模块，有点像 Android 那样，支持添加显示新的页面、返回、跳到之前显示过的页面等。正打算使用这里提到的这个 QStackedWidget 的扩展来实现，还有不错的页面切换效果哦。之后再写个博文好好讲讲这个页面管理模块。

杰良-20140803
