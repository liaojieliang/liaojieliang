---

layout: default

title: 杰良

---

### 个人简介
- 2013 年毕业于华南农业大学，计算机科学与技术专业。
- 擅长嵌入式 Linux 软件开发，喜好科幻，爱阅读。
- 曾就职于[广州英码](http://www.ema-tech.com/)、[京信通信](http://www.comba.com.cn/)，目前在[广州创龙](http://www.tronlong.com/)效力。

### 其他网络栖息地
- [豆瓣](http://www.douban.com/people/engrossment/)
- [CSDN](https://blog.csdn.net/engrossment)

### 更新 - 20201214
当前博客已停更，已转移到[语雀](https://www.yuque.com/liaojieliang)