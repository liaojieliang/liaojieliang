---

layout: post

title: WordPress首页分类排序

categories: software-development

---

同样是有修改代码和安装插件两种办法。由于现在对php还一点不熟，就偷懒使用插件解决问题了。用google搜索，较多人都建议安装这个插件：

>order-categories

我到官网一搜，那里提示这个插件已经超过2年没有更新了。于是选择了另外一个：
>Category Order and Taxonomy Terms Order

下载，解压，复制到对应目录下（wp-content/plugins/），然后更新代码。在网页后台“文章”一栏下就可以看到这个插件了，可以通过拖拽排序分类，十分方便。

另外一些直接修改代码的方法。下面这个链接里面提供了一个很简单的方法，修改后分类会根据创建的先后排序。不够灵活。

[http://www.datacentersky.com/taught-you-how-to-adjust-the-wordpress-the-category-column-sort.html](http://www.datacentersky.com/taught-you-how-to-adjust-the-wordpress-the-category-column-sort.html)

下面这个链接里提供了比较复杂的修改代码的方法：

[http://www.salontino.net/index.php/archives/154](http://www.salontino.net/index.php/archives/154)

2013-04-18-杰良
