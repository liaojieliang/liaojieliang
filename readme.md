## About

This is the data for my personal site. Using Gitee Pages and Jekyll.

## License

The following directories and their contents are licensed under [by-nc-sa/4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/):
- _posts/
- images/

All other directories and files are MIT Licensed.

## Thanks

Thanks to [Jekyll](http://jekyllcn.com/) and Gitee!
