---

layout: post

title: "单件模式（Singleton）简介"

categories: software-development

---

单件模式保证一个类只有一个实例，并提供一个访问它的全局访问点。

#### 1、为什么需要单件模式

对于有些类来说，它的作用范围是全局性的，更重要的是，它的数据也是全局性的。这里说数据全局性并不是指全局变量，而是说全局所有调用这个类的方法都是操作同一组数据。也即是同一个实例。这看上去有点像静态方法，但对于静态方法，每次相同的输入会得到相同的输出。而单件模式并不是这样，它是有实例对象的，这个对象是“活”的，而不是“静态”的。

#### 2、示例（C++代码）

定义：

```
class Singleton {
public:
    static Singleton *Instance();
protected:
    Singleton();
private:
    static Singleton *_instance;
};
```

实现：

```
Singleton *Singleton::_instance = 0;

Singleton *Singleton::Instance() {
    if (_instance == 0) {
        _instance = new Singleton;
    }
    return _instance;
}
```

这里是最简明的示例，可根据需要添加数据和方法成员。值得注意的是，构造函数是被声明为protected的，这表示不能够直接实例化该类为一个对象，然后使用。要想使用该类提供的方法，必须通过其静态方法Instance来调用。例如这样：Singleton::Instance->run()，（假设其声明定义了一个叫run的成员函数）。

这里使用了惰性（lazy）初始化。由于不能通过直接声明对象来实例化，所以实例化会发生在第一次调用Instance的时候。自然，如果一直没有调用Instance就从不会被实例化。

#### 3、单件模式的优点

3.1、对唯一实例的受控访问。相对应的，如果使用声明全局对象的方法，就危险的多。

3.2、缩小命名空间。它没有声明全局变量，也就没有污染全局变量的命名空间。

3.3、允许对操作和表示的精化。通过继承Singleton类，精细其操作，但是可以使得原有接口不变。可以在运行时刻通过配置改变Instance返回的是某一个子类的实例。

参考：Gof 的[《设计模式》](http://book.douban.com/subject/1052241/)

2013-04-26-杰良
