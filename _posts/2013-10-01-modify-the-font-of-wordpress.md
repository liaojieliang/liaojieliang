---

layout: post

title: "修改 Wordpress 显示的字体"

categories: software-development

---

博客字体是样式的一部分，也就是由当前使用的主题来决定的。所以我们的第一步就是要找到对应主题的样式文件 —— style.css ，开始作手动修改。搜索找到 body 块，可以看到 font-size，font-family 等属性。BTW 如果觉得博客显示的字体大小不合适，就可以修改 font-size 。font-family 后面是一个字体列表，主题会从第一个开始使用字体，如果出错就选择下一个。所以我们要做的就是更改顺序或者加入新的字体。比如要使用微软雅黑，在 font-size 后面加入 Microsoft YaHei ，当然别忘了英文逗号。

我的实际操作有点特别，修改后没有生效。所以我又修改了下面的 .body.custom-font-enabled 里的 font-family 。保存后刷新博客，马上就看到了漂亮的微软雅黑了！

2013-10-01-杰良
