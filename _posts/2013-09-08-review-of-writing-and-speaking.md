---

layout: post

title: "格雷厄姆的《Writing and Speaking》"

categories: thinking-writing

---

[《Writing and Speaking》](http://paulgraham.com/speak.html)

一些摘抄与点评：

1、“it was a revelation to me how much less ideas mattered in speaking than writing”。就演说与写作对比来说，演说的速度快，通常是脱口而出，自然就来不及进行足够的细致思量。所以就没有那么多那么深的思想在其中。当然，这只是单单从内容的角度来对比的。面对面的演说比阅读文章更能调动受众的积极性，使其进行快速的思考，更好地接受。

2、“The way to get the attention of an audience is to give them **your** full attention, and when you're delivering a prewritten talk your attention is always divided between the audience and the talk—even if you've memorized it”。这就是为什么课堂上照本宣科的老师十分容易让学生走神。老师自己本身也走神了或者只是沉浸在课前准备的材料上，根本就没有给予受众足够的关注。

3、“Occasionally the stimulation of talking to a live audience makes you think of new things, but in general this is not going to generate ideas as well as writing does, where you can spend as long on each sentence as you want”。如果不想照本宣科，就要激发自己去想出些新的内容出来，这也算是一种创作的过程。但相对写作来说，含金量是较低的。在写作时，你能够在写下每一个句子时，给出足够的思考时间，甚至还可以去查阅资料。文字作品可以精打细造。

4、“All the time you spend practicing a talk, you could instead spend making it better”。通过不断的演练你能够呈现一个精彩的演说，包含很多精辟的思想。如果没有太多的时间或者根本就很不善于演说，那就不如把花在演练的时间用来修改演说的内容，让其更丰富。我更希望之后，人们记得我说了什么，而不是我怎么说的。

5、“Any given person is dumber as a member of an audience than as a reader”。同样地，作为一个观众没有作为一个读者那么多的思考理解的时间。

通篇看下来，这篇文章讲的是演说与写作的对比，但还有更多的是怎么样能够讲得更好。我个人来说，与作者有着同样的倾向，希望自己成为一个更好的写作者多于希望成为更好的演说着。因为我热爱着思考。

2013-09-08-杰良
