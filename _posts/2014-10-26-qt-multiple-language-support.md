---

layout: post

title: "Qt 的多国语言支持"

categories: software-development

---

## 基本步骤
* 所有需要显示的文本使用 tr 转换再使用。
* 生成翻译文件并编辑翻译的文本。
* 需要转换的时候加载翻译文件，安装 translator。重新设置已经创建的 UI 元素的文本。

## 命令
* 生成翻译文件: xx.ts
    * lupdate app.pro
* 由翻译文件生成最终使用的文件: xx.qm
    * lrelease app.pro

## 注意
* 在 pro 文件编辑 TRANSLATIONS 变量
    * TRANSLATIONS += language/en.ts
* 不能传递变量给 tr，除非用文本初始化变量前先用 QT_TR_NOOP 宏标记文本字符串，并且，该变量要声明于 QObject 类或其子类的内部。
* 为了获得更好的支持，把 tr 更改为使用 trUtf8。起码生成的翻译文件能够正常显示中文了。
* 如果不需要动态更改语言，在程序启动时根据设置加载特定的语言文件即可。

参考：[How to create a multi language application](https://qt-project.org/wiki/How_to_create_a_multi_language_application)


杰良-2014-10-26
