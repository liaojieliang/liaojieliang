---

layout: post

title: "高考"

categories: others

---

前天晚上梦回初三，新学期开学。差点眼泪都掉出来。

今天 6 月 7 号，高考第 1 天。QQ 群上有同学分享了一张照片，说四川某高考考场临开考 20 分钟有一位女生因经痛晕倒，另一男生抱起她就往医院跑，最终满头大汗地跑回考场参加考试。我几乎要击掌称快，该男生干得漂亮！看着那照片，我就看出大将风范来。

回想 4 年以前，我即将参加高考。紧张地复习了好几个月了，所以临考前几天也不想再做什么准备，只是缓慢地翻翻资料。看不出来有多大的压力。事实上，已经变得沉默寡言了，面无表情。脑里什么也不想，比较空白。现在回想起来，这该是压力过大时的一种自我保护机制吧。

所以我是觉得，当时的我是无论如何也做不到想今天成都的那位男生一样的。处事泰然自若，关键还是自主，有所为。试想当时的情景，那晕倒的女生的照顾，一般是交给监考老师来做的。但他认为她是对自己很重要的人，无论什么情况下都应该挺身而出。说到底，他还是可以通过奔跑得满头大汗来同时做到照顾好她和不耽误高考。

2013-06-07-杰良
