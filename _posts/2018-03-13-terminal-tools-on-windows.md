---

layout: post

title: "Windows 下的终端工具 putty 与 xshell"

---

## ~~0、SecureCRT~~
使用破解版的 SecureCRT 一来存在木马植入等风险，更重要的是使用破解软件不道德甚至违法。

## 1、putty
[https://putty.org/](https://putty.org/)
* 优点：开源、完全免费、小巧精悍。还有很多基于 putty 进行扩展增强的终端工具。
* 缺点：不支持多标签页。不支持保存登录密码。

## 2、byobu
对于 putty 不支持多标签页的问题，一种缓解的办法是在目标主机 Ubuntu 中安装 byobu（http://byobu.co/），通过 putty 登录该 Ubuntu 后，启动 byobu，使用 F2 就可以很方便创建出多个虚拟终端出来。使用 F3、F4 进行切换使用。

安装方法：HOST#  sudo  apt-get  install  byobu

### 2.1、putty 与 byobu 的 Fn 键冲突问题
注意在 Windows 上的 putty 使用 Fn 键可能无法发送到 Ubuntu，需要在 putty 设置中做如下配置：配置“终端”->“键盘”->“功能键类型”，设置为 Xterm R6 。

## 3、xshell
* 不像 putty 由个人开发者开发并有志愿者社区维护，xshell 由一个商业公司开发，但提供了免费版本。
* xshell 的功能很强大，近似于 SecureCRT。
* 在其[产品页面](http://www.netsarang.com/products/xsh_overview.html)点击 download，跳转的页面中选择 Home & School user 授权协议，填上用户名、邮箱，然后提交。就会收到一封带有下载链接的邮件。
* 推荐配置
    * 菜单栏“工具” -> “选项”全局配置：选中即复制、右键即粘贴。
    * 菜单栏“文件” -> “属性”会话配置：缓冲区大小 102400 行、Consolas 字体 14 号。
    * 为释放更多空间用于显示终端内容，去掉菜单栏显示。

2018-03-13
