---

layout: post

title: "近来所做的事"

categories: others

---

打算从去年找工作开始说起。

听师兄提起过深信服公司，然后查了资料，感觉不错。所以第一个目标就是参加这家公司的招聘会，争取拿到 offer 。结果第一次笔试就铩羽而归。最后是建锋和师兄拿到了其 offer 。我也对这事儿一直耿耿于怀，信心也低了不小。师兄把这些都看在眼里。那天英码公司来学校开宣讲会，我并没有去参加。但是师兄在结束后找到我并亲自把我介绍过去，于是我就抱着试试看的心态投了简历然后笔试面试。结果拿到 offer 了，这是我的第二个 offer 。另一个的工作地点是在珠海郊区，不想去，所以选了英码。

我在英码的直属项目经理 Terry 让我尽早去开始实习，推了一段时间，最后是从 11 月 19 日开始上班实习。一开始接触的是公司内部自己要做的 esg 项目，也是刚刚才开始的。我主要负责图像的旋转模块，用到了修饰者模式——这也是我第一次接触设计模式，觉得很神奇有趣。之后大概过了一个多星期，开始另一个项目——生产测试框架——的开发，所以我完成旋转模块后就再没接触过 esg 项目了。最后也不知道它做得怎么样，这个，是我的缺点，不够主动关心身边同事的事情。然后生产测试框架的开发就成了我的工作重点，直到这个月初。先后实现了 Perl 版和 C++ 版。Perl 版前后从无到有到完善，然后内部发布，用了两个多月的时间。发布后就在公司的制造部有断断续续的使用了。但是 Terry 最后发现，Perl 版的生产测试框架使用了一些外部模块，这给配置环境带来了极大的麻烦。在一次经理会议之后，Terry 回来兴致勃勃地跟我说，我们要推倒重来！用 C++ 来做，并做成一个商业级产品。我当时也没多想，老大说怎么样就怎么样吧。那时候就是想问题只看表面，从不懂得深挖一点看本质。不过虽然现在知道要去思考了，但还是看不深，继续锻炼吧。这次倒了过来，我来实现 UI 部分，而 Terry 去做最重要的解析器。Perl 版的解析器是我编码实现的，虽然基本设计都是由 Terry 来做。对 Perl 版的 UI 完全不懂。但是在做 C++ 版的 UI 时要参考 Perl 版的，于是就返回去看了一遍，基本搞懂了其原理。虽然依据我毫无经验的判断，这个 C++ 版的开发有点太慢，但是整个过程是持续而平稳的。这里有一些项目规划管理的工作我没有在表面上看到，通常我只是根据 Terry 的安排，按部就班地去做。

总结来说，前期写 Perl 版，学习了 Perl 语言。包括 Perl 的基本语法，Perl 的继承，还有模块的使用。另外接触了 xml 语言，使用 Perl 的 Lib::XML 模块进行解析。还有的就是工厂模式的使用。到后期由于要写 UI，接触了 SDL。对 C++ 的使用也变熟悉了一点。

2013-09-29-杰良
