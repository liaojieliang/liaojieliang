---

layout: post

title: "移植 MQTT client paho 到 omapl138"

---

## 概述
根据上篇《移植 MQTT broker mosquitto 到 omapl138》移植好 MQTT 的 server 后，下一步就是移植客户端了。下面介绍两个客户端的移植安装。可以按需选择。

其实这里介绍的两个都是 eclipse 实现的 paho.mqtt，只是不同语言的版本。

## python 版本 paho-mqtt
首先在 omapl138 上可以基于原有的 python 2.7 环境直接安装 paho-mqtt 模块，然后就可以写 python 程序使用 paho 与 mosquitto broker 进行通信了。

步骤：
 * 准备 python 环境。由于我正在使用的 omapl138 文件系统上的 python 2.7 缺乏 setuptool 等模块，需要先安装。
    * 更新opkg。Target#  opkg update
    * 为简便起见，直接安装全部模块。Target#  opkg install python-modules
 * 下载 paho.mqtt 源码包：https://pypi.python.org/pypi/paho-mqtt/1.1
 * 发送到 138 板卡并解压。
 * 进入到源码包。Target#  python setup.py install

完成安装！

## C 版本 paho.mqtt.c
如果需要用 C 语言进行程序开发，那就要移植 C 版本的 MQTT 客户端了。我们同样选择了 paho，对标准的支持性很好。

步骤：
 * 在 Ubuntu 安装文档生成的相关工具。Host#  sudo apt-get install doxygen graphviz
 * 进入 138 开发环境。Host#  source /home/jack/omapl138/ti/mcsdk_1_01_00_02/linux-devkit/environment-setup
 * 下载源码包。[https://github.com/eclipse/paho.mqtt.c/releases](https://github.com/eclipse/paho.mqtt.c/releases)
 * 解压并进入源码包。Host#  cd paho.mqtt.c-1.2.0  &&  mkdir  build  &&  cd build
 * 修改交叉编译 toolchain 配置。Host#  vim ../cmake/toolchain.linux-arm11.cmake 修改 SET(CMAKE_C_COMPILER arm-arago-linux-gnueabi-gcc)
 * 执行 cmake。Host#  cmake -DCMAKE_INSTALL_PREFIX=target -DPAHO_WITH_SSL=FALSE -DPAHO_BUILD_SAMPLES=TRUE -DPAHO_BUILD_DOCUMENTATION=TRUE -DCMAKE_TOOLCHAIN_FILE=../cmake/toolchain.linux-arm11.cmake ..
 * 执行编译。Host#  make && make install
 * 生成的目标文件在 target 目录。这里的文件可以用作交叉编译生成目标程序放到 omapl138 上运行，同时需要复制一份放到 omapl138 的  /usr 下。

完成移植！

2018-01-09
